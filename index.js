const fs = require("fs");
const express = require("express");
const app = express();

const data = require("./users.json")

app.use(express.static("./public"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.set("view engine", "ejs");

const Regvalidation = (req, res, next) => {
    const {username} = req.body;
    const isFind = data.find((row) => row.username == username);
    if (isFind) {
        res.status(400).json({message: "Username not available"});
    } else {
        next();
    }
};

const Loginvalidation = (req, res, next) => {
    const {username, password} = req.body;
    const isFind = data.find((row) => row.username == username);
    if (isFind) {
        const Passvalid = data.find((row) => row.password == password);
        if (Passvalid) {
            next();
        } else {
            res.status(400).json({message: "Wrong password"});
        }
        next();
    } else {
        res.status(400).json({message: "Username not registered"});
    }
};

app.get("/", (req, res) => {
    res.render("home.ejs");
});

app.get("/game", (req, res) => {
    res.render("Game.ejs");
});

app.get("/register", (req, res) => {
    const {username, password} = req.query;
    res.render("register.ejs", {username, password});
});

app.post("/register", Regvalidation, (req, res) => {
    const {username, password} = req.body;
    const users = [...data, {username, password}]
    fs.writeFileSync("./users.json", JSON.stringify(users));
    res.redirect(`/home?username=${username}`);
});

app.get("/login", (req, res) => {
    const {username, password} = req.query;
    res.render("login.ejs", {username, password});
});

app.post("/login", Loginvalidation, (req, res) => {
    const {username} = req.body;
    res.redirect(`/home?username=${username}`);
});

app.get("/home", (req, res) => {
    const {username} = req.query;
    console.log(`Hello, ${username}`)
    res.render("userhome.ejs", {username});
});

app.get("/users", (req, res) => {
    res.send(data);
});

app.get("*", (req, res) => {
    console.log("ERROR 404 - File not found")
    res.status(404).render("404.ejs");
});

app.listen(8000, () => console.log(`listening at http://localhost:${8000}`));