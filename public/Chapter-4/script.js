const rockBtn = document.getElementById("rock");
const paperBtn = document.getElementById("paper");
const scissorBtn = document.getElementById("scissor");
const selection = ["rock", "paper", "scissor"];

function choice(pilihan) {
    const choiceNumber = Math.floor(Math.random()*3);
    const computerChoice = selection[choiceNumber];
    const playerChoice = pilihan;
    console.log(`your choice is ${playerChoice}`);
    console.log(`computer choice is ${computerChoice}`);
    winner(playerChoice,computerChoice);
    const text = document.getElementById("text");
    text.remove();
    const allchoice = document.querySelectorAll(".choice");
    allchoice.forEach((resetChoice) => {
        resetChoice.setAttribute("style", "background-color:null;");
    })
    const yourChoice = document.getElementById(`choice${playerChoice}`);
    yourChoice.setAttribute("style", "background-color:#C4C4C4; border-radius: 30px;");
    const comChoice = document.getElementById(`comchoice${computerChoice}`);
    comChoice.setAttribute("style", "background-color:#C4C4C4; border-radius: 30px;");
}

function win(){
    const win = document.createElement("h1");
    win.textContent = "PLAYER 1 WIN";
    win.setAttribute("class", "win");
    win.setAttribute("id","text");
    element.append(win);
    console.log("PLAYER 1 WIN");
}

function lose(){
    const lose = document.createElement("h1");
    lose.textContent = "COM WIN";
    lose.setAttribute("class", "lose");
    lose.setAttribute("id","text");
    element.append(lose);
    console.log("COM WIN");
}

function draw(){
    const draw = document.createElement("h1");
    draw.textContent = "DRAW";
    draw.setAttribute("class", "draw");
    draw.setAttribute("id","text");
    element.append(draw);
    console.log("DRAW");
}

const winner = (player,computer) => {
    if(player === computer){
        draw();
    }
    else if(player == 'rock'){
        if(computer == 'paper'){
            lose();
        }else{
            win();
        }
    }
    else if(player == 'scissor'){
        if(computer == 'rock'){
            lose();
        }else{
            win();
        }
    }
    else if(player == 'paper'){
        if(computer == 'scissor'){
            lose();
        }else{
            win();
        }
    }
}